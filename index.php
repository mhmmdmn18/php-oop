<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>PHP OOP</title>
</head>
<body>
	<?php
	require ('animal.php');
	require ('Ape.php');
	require ('Frog.php');
		
	// Release 0
	$sheep = new Animal("shaun");
	echo "Name : " . $sheep->name . "<br>"; // "shaun"
	echo "legs : " . $sheep->legs . "<br>"; // 4
	echo "cold blooded : " . $sheep->cold_blooded . "<br><br>"; // "no" 

	// // Release 1
	$kodok = new Frog("buduk");
	echo "Name : " . $kodok->name . "<br>"; // "buduk"
	echo "legs : " . $kodok->legs . "<br>"; // 4
	echo "cold blooded : " . $kodok->cold_blooded . "<br>"; // "no" 
	$kodok->jump() ; // "Hop Hop"
	echo "<br><br>";

	$sungokong = new Ape("kera sakti");
	echo "Name : " . $sungokong->name . "<br>"; // "kera sakti"
	echo "legs : " . $sungokong->legs . "<br>"; // 2
	echo "cold blooded : " . $sungokong->cold_blooded . "<br>"; // "no" 
	$sungokong->yell() // "Auooo"
	?>
</body>
</html>